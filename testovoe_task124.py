import pandas as pd
import random as rd
import datetime as dt
import dateutil.relativedelta as dt_rel
import sys
import psycopg2
from sqlalchemy import create_engine
import plotly.express as px

def random_date(start, end):
    delta = end - start
    random_day = rd.randint(0, delta.days)
    return start + dt.timedelta(days=random_day)

def main(start = dt.datetime.today().date() - dt_rel.relativedelta(years=rd.randrange(3,6)), end = dt.datetime.today().date(), n_month=1):
    df = new_df(start, end, n_month)
    return df

def new_df(start, end, n_month=1):
    if n_month < 0:
        print("Количество месяцев должно быть > 0")
        sys.exit()
    else:
        df = pd.DataFrame(columns=['UserId', 'PaymentAmount', 'PaymentDate'])
        user_id = rd.getrandbits(16)
        delta_dates = dt_rel.relativedelta(end, start)
        cnt_month = delta_dates.years*12 + delta_dates.months
        for i in range (0, 100):
            j = rd.randint(5,50)
            if n_month > j:
                n_month = j
            for x in range(n_month - 1, j):
                if x == 0 or x > n_month - 1:
                    df.loc[len(df.index)] = [user_id, rd.randint(100,10000), random_date(start, end)]
                else:
                    month = []
                    while len(month) < n_month:
                        n = rd.randint(0, cnt_month-1)
                        if n not in month:
                            month.append(n)
                    for new_month in month:
                        df.loc[len(df.index)] = [user_id,  rd.randint(100,10000), start + dt_rel.relativedelta(months=new_month)]                
            user_id += 1
    return df

def connection(df,login = 'postgresql://postgres:123456@localhost:5432/testanalytics'):
    conn = create_engine(login)
    df.to_sql('task2', conn, if_exists='replace')
    return conn

def main(start = dt.datetime.today().date() - dt_rel.relativedelta(years=rd.randint(3, 5)), end = dt.datetime.today().date(), n_month=1):
    df = new_df(start, end, n_month)
    return df

if __name__ == "__main__":
    df = main(n_month=3)
    conn = connection(df=df)

df_task4 = pd.read_sql(
    'select month,ad."UserId",sum(tn."PaymentAmount") as sum from task3 ad left join task2 tn \
     on ad."UserId" = tn."UserId" \
     and date_trunc(\'month\',tn."PaymentDate") = ad.month \
     group by ad."UserId", ad.month', conn).fillna(0)
df_task4['MRR_comulative'] = df_task4.groupby(['UserId'], as_index=False)['sum'].cumsum()

df_task4_top = df_task4.groupby(['UserId'], as_index=False).agg({'MRR_comulative':'max'}).sort_values('MRR_comulative', ascending=False).head(10)['UserId']

df_task4 = df_task4.merge(df_task4_top, how='inner', on='UserId')
df_task4 = df_task4.pivot_table('MRR_comulative', 'month', 'UserId').fillna(0).reset_index()

fig = px.line(df_task4, x=df_task4.month, y=df_task4.columns)
fig.show()